
let toggleId = document.getElementById("checkbox");
let basicPriceId = document.getElementById("basicId");
let professionalPriceId = document.getElementById("premiumId");
let masterPriceId = document.getElementById("masterId");

function setPrices(isAnnual) {
    if (isAnnual) {
        basicPriceId.textContent = "19.99";
        professionalPriceId.textContent = "24.99";
        masterPriceId.textContent = "39.99";
    } else {
        basicPriceId.textContent = "199.99";
        professionalPriceId.textContent = "249.99";
        masterPriceId.textContent = "399.99";
    }
}

toggleId.addEventListener("click", () => {
    setPrices(toggleId.checked);
    localStorage.setItem("isAnnual", toggleId.checked);
});

document.addEventListener("DOMContentLoaded", () => {
    const isAnnual = JSON.parse(localStorage.getItem("isAnnual")) || false;
    toggleId.checked = isAnnual;
    setPrices(isAnnual);
});
